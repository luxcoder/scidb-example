package de.thevinci.scidb;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Example {

	public static void main(String[] args) {
		// load driver
		try {
			Class.forName("org.scidb.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		}
		// get connection
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:scidb://localhost/");
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		// get mini dateset file
		String filename = args[0];
		System.out.println("Importing file '" + filename + "'");
		// create flat array for load
		String query = "CREATE ARRAY test <time:int64,lon:int64,lat:int64,temp:int32 > [i=0:*,1000000,0]";
		try {
			Statement stmt = connection.createStatement();
			System.out.println("Executing query: " + query);
			stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		// load array
		query = "LOAD test FROM '%s' AS '(int64,int64,int64,int32)'";
		query = String.format(query, filename);
		try {
			Statement stmt = connection.createStatement();
			System.out.println("Executing query: " + query);
			stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}	
	
}
